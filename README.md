# React Presentation
By Satinderjeet Singh <satinderjeet.singh@publicisna.com> | July 16, 2019
___


## Downloading & starting the application.
### Requirements
- NodeJS - ^10.16
- NPM - ^6.4

Starting the Presentation Application:
- First Clone the repository: `git clone git@bitbucket.org:satin_jeet/react-presentation.git`.
- Install dependencies: `npm i`.
- Start the server: `node server/serve.js`
- Use Arrow keys <- & -> to switch between next & the previous slide.
___
## Directory Structure:

### `./server`

Contains the code for server in order to serve:
- The static react application files for presentation.
- **Alarm.mp3** file.
- Handling the root & all the nested routes to render the application.
- Start the bundler to compile & watch the source files for changes.

### `./src`
Contains main entry file for react application & it's styles (`index.html`).
Parcel bundler parses `index.html` to find the source files & compile ( & serve ). In this case the files are `./src/index.tsx` & `./src/slides/styles/index.scss`.

