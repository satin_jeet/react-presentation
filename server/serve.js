const express = require("express");
const app = express();
const fs = require("fs");
const path = require("path");
const Bundler = require("parcel-bundler");

app.get("/", (req, res) => {
    res.sendFile(path.resolve(__dirname, "..", "dist/index.html"));
});
app.get("/alarm.mp3", (req, res) => {
    res.sendFile(path.resolve(__dirname, "alarm.mp3"));
});
app.get("/slide-*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "..", "dist/index.html"));
});
app.use('/public', express.static(__dirname + "/../dist"));

bundler = new Bundler(__dirname + "/../src/index.html", {publicUrl: '/public'});

app.listen('12345', '0.0.0.0', async () => {
    console.log("Starting server on http://0.0.0.0:12345");
    const bundle = await bundler.bundle();
});
