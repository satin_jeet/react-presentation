import React from "react";
import { SlideHeader } from "./common/slide-header";
import { SlideContent } from "./common/slide-content";
import { Slide } from "./common/slide";
import image from "./common/says.png";

export function WhatIsReact() {
    return <Slide>
        <SlideHeader>What is React</SlideHeader>
        <SlideContent>
            <div className="col s6">
                <h5 className="center-align">What it says on the very fist page <a href="https://reactjs.org/">https://reactjs.org/</a></h5>
                ￼<img src={image} className="materialboxed" width="100%" />
            </div>
            
            <div className="col s6">
                <h5 className="center-align">How it is used <b>most</b> of the time.</h5>
                ￼<img src="https://camo.githubusercontent.com/29765c4a32f03bd01d44edef1cd674225e3c906b/68747470733a2f2f63646e2e7261776769742e636f6d2f66616365626f6f6b2f6372656174652d72656163742d6170702f323762343261632f73637265656e636173742e737667" className="materialboxed" width="100%" />
            </div>
        </SlideContent>
    </Slide>
}