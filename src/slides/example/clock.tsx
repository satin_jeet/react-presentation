import React, { Component } from "react";

interface IClockProps {
    alarmHour: number,
    alarmMinute: number,
    onAlarm: (disableAlarm: () => void) => void;
}

export class Clock extends Component<IClockProps> {
    private _hour: number = 0;
    private _minute: number = 0;
    private timer = -1;

    state = {
        time: new Date(),
        alarmActive: false
    }

    constructor(props: IClockProps) {
        super(props);
        this._hour = props.alarmHour;
        this._minute = props.alarmMinute;
        console.log(`Alarm requested at: ${this._hour}:${this._minute}`);
    }

    componentDidMount() {
        clearInterval(this.timer);
        this.setTimer();
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    componentWillUpdate() {
        const m = this.state.time.getMinutes();
        const h = this.state.time.getHours();

        if (!this.state.alarmActive) {
            if (m == this._minute && h == this._hour) {
                console.log('Activating Alarm');
                this.props.onAlarm(this.snooze);
                this.setState({ alarmActive: true });
            }
        }
    }

    render() {
        return <div className="row">{
                this.padLeft(this.state.time.getHours())
            }:{
                this.padLeft(this.state.time.getMinutes())
            }:{
                this.padLeft(this.state.time.getSeconds())
            }
            <br />
            <span className="grey-text lighten-3 small-text-for-alarm">
                <i className="material-icons">access_alarm</i>
                {
                    this.padLeft(this._hour)
                }:{
                    this.padLeft(this._minute)
                }
            </span>
        </div>
    }

    private snooze = () => {
        console.log('Snoozing Alarm');
        this._minute += 1;
        if (this._minute == 60) {
            this._hour++;
            this._minute++;

            if (this._minute == 24) {
                this._minute = 0;
            }
        }
        this.setState({ alarmActive: false });
    }

    private setTimer = () => {
        this.timer = setInterval(() => {
            this.setState({ time: new Date() });
        }, 1000);
    }

    private padLeft(num: number): string {
        return `0${num}`.slice(-2);
    }
}