import React from "react";
import { Clock } from "./clock";

export class ClockTower extends React.Component<any> {
    private audioTag: HTMLAudioElement;
    state: {
        alarmFired: boolean;
        disableFn: undefined | (() => void)
    } = {
        alarmFired: false,
        disableFn: undefined
    }

    private alarmDate = new Date(new Date().getTime() + 60000); 

    render() {

        return <div className="col s12">
            {
                this.state.alarmFired && <b className="red">Alarm !!</b>
            }
            
            <Clock alarmHour={this.alarmDate.getHours()} alarmMinute={this.alarmDate.getMinutes()} onAlarm={this.alarmHandler} />
            <audio src="/alarm.mp3" loop style={{display: "none"}} ref={el => {
                this.audioTag = el as HTMLAudioElement
            }}/>

            {
                this.state.disableFn && <button onClick={() => {
                    this.audioTag && this.audioTag.pause();
                    this.state.disableFn && this.state.disableFn();
                    this.setState({ disableFn: undefined, alarmFired: false });
                }}>Clear</button>
            }
        </div>
    }

    private alarmHandler = async (disableFn: () => void) => {
        if (this.audioTag.paused) {
            this.audioTag && await this.audioTag.play();
        }
        this.setState({ disableFn, alarmFired: true });
    }
}