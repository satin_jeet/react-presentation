import React from "react";
import { SlideHeader } from "./common/slide-header";
import { SlideContent } from "./common/slide-content";
import { Slide } from "./common/slide";
import image from "./common/reactcomponent.svg";
import image2 from "./common/component.svg";
import { CodePre } from "./common/codepre";

export function WhatReactIs() {
    return <Slide>
        <SlideHeader>What is React then?</SlideHeader>
        <SlideContent>
            <div className="col s4" style={{float: 'left'}}>
                <h5>Declarative</h5>
                <p>React makes it painless to create interactive UIs. Design simple views for each state in your application, and React will efficiently update and render just the right components when your data changes.
                Declarative views make your code more predictable and easier to debug.</p>
                ￼<img src={image} className="materialboxed" width="100%" />
            </div>
            <div className="col s8" style={{float: 'right'}}>
                <h5>Learn Once, Write Anywhere</h5>
                <p>We don’t make assumptions about the rest of your technology stack, so you can develop new features in React without rewriting existing code.
                React can also render on the server using Node and power mobile apps using React Native.</p>
                <CodePre>
                        {
`
React:
    class Foo extends Component {
        // Do something during the lifecycle
        render() {
            return <div>
                <input ... />
                <input ... />
                <p>
                    <...>
                </p>
            </div>
        }
    }

React-native:
    class Foo extends Component {
        // Do something similar during the lifecycle on mobile device
        render() {
            <View>
                <TextInput ... />
                <TextInput ... />
                <Text>
                    <...>
                </Text>
            </View>
        }
    }
`
                        }
                </CodePre>
            </div>
            <div className="col s4" style={{float: 'left'}}>
                <h5>Component-Based</h5>
                <p>Build encapsulated components that manage their own state, then compose them to make complex UIs.
                Since component logic is written in JavaScript instead of templates, you can easily pass rich data through your app and keep state out of the DOM.</p>
                ￼<img src={image2} className="materialboxed" width="100%" />
            </div>
        </SlideContent>
    </Slide>
}