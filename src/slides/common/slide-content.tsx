import React from "react";

export function SlideContent(props: { children?: any }) {
    return <div className="row col s12">
        { props.children }
    </div>
}