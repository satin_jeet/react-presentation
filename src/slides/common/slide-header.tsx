import React from "react";
// import img from "../styles/logo.svg"
type SlideHeaderChildren = ( JSX.Element | string )[];

export function SlideHeader(props: {
    children: SlideHeaderChildren;
    className?: string;
    noIcon?: boolean;
}) {
    const c = props.className || "";
;    return <div className={`row col s12 slide-header ${c}`}>
        <h2 className="center-align">
            { 
                !props.noIcon && <i className="r-signed"></i>
            }
            { props.children }
        </h2>
    </div>
}