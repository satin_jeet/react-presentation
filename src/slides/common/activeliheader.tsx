import React from "react";

export function ActiveIfHeader(props: { children: string }) {
    return <h5 className="active-if-header">{props.children}</h5>
}

export function ActiveIfContent(props: { children: any }) {
    return <span className="active-if-content row col s12">{props.children}</span>
}

export function ActiveIfP(props: { children: any, className?: string }) {
    return <p className={`active-if-p ${props.className || ''}`}>{props.children}</p>
}