import React from "react";
import "prismjs";
// import "prismjs/plugins/line-numbers/prism-line-numbers";
// import "prismjs/plugins/line-highlight/prism-line-highlight";
import "prismjs/components/prism-jsx";
import "prismjs/components/prism-tsx";
import PrismCode from 'react-prism';

export function CodePre(props: { children: string | string[], className?: string, lang?: string }) {
    const lang = props.lang || 'jsx';
    return <pre className={`${props.className ? props.className: ''}`}>
        <PrismCode className={`language-${lang}`}>
            { props.children }
        </PrismCode>
    </pre>
}