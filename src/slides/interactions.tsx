import React, { Component } from "react";
import { Slide } from "./common/slide";
import { SlideHeader } from "./common/slide-header";
import { SlideContent } from "./common/slide-content";
import { LiActivateIf } from "./subslides/jsxslide";
import { ActiveIfHeader, ActiveIfContent, ActiveIfP } from "./common/activeliheader";
import { CodePre } from "./common/codepre";

const backKeys = ["ArrowLeft", "Escape", "Backspace"];
const nextKeys = ["ArrowRight", "Space", " ", "Enter"];

export class ComponentInteractions extends Component<any> {
    state: {
        subPointer: number
    } = {
        subPointer: 0
    }

    private keyHandler = (e: KeyboardEvent) => {
        let newSubPointer = this.state.subPointer;
        if (backKeys.indexOf(e.key) >= 0) {
            newSubPointer--;
        } else if (nextKeys.indexOf(e.key) >= 0) {
            newSubPointer++;
        }

        if (newSubPointer >= 0 && newSubPointer <= 3) {
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
        }

        this.setState({ subPointer: newSubPointer });
    }

    componentWillMount() {
        document.body.addEventListener('keydown', this.keyHandler);
    }

    componentWillUnmount() {
        document.body.removeEventListener('keydown', this.keyHandler);
    }

    render() {
        return <Slide>
            <SlideHeader>Component Interactions</SlideHeader>
            <SlideContent>
                <ol className={'top-ol no-number'}>
                    <li className={this.state.subPointer < 2 ? `activated`: ``}>
                        <b>User Interaction</b>
                        <ul>
                            <LiActivateIf expectedPage={0} page={this.state.subPointer}>
                                <ActiveIfHeader>How to</ActiveIfHeader>
                                <ActiveIfContent>
                                    <ActiveIfP>To handle user interaction with the UI Elements, React provides a very similar syntax, as defined by HTML, to bind events to the react element.</ActiveIfP>
                                    <CodePre className="col s6">
                                        {`\n// HTML\n<a class="some button class" onclick="handleBtnClick()">\n    Click Me\n</a>\n\n`}
                                    </CodePre><CodePre className="col s6">
                                        {`// JSX\n function handleBtnClick(args) { ...do something };\n\n<a class="some button class" onClick={handleBtnClick}>\n    Click Me\n</a>`}
                                    </CodePre>
                                    <ActiveIfP>
                                        The differences being, using camel case in jsx rather than all lower case and using function reference for the event handler, not a string.
                                    </ActiveIfP>
                                </ActiveIfContent>
                            </LiActivateIf>
                            <LiActivateIf expectedPage={1} page={this.state.subPointer}>
                                <ActiveIfHeader>More about events.</ActiveIfHeader>
                                <ActiveIfContent>
                                    <ActiveIfP>- Using the JSX syntax, there is no need to write manual code to bind events using <code>addEventListener</code>.</ActiveIfP>
                                    <ActiveIfP>- JSX binds the events after creating the element automatically.</ActiveIfP>
                                    <ActiveIfP>- Events only remain bound to the elements or the tree targetted, eliminating the need to find the correct node to bind with the interactions.</ActiveIfP>
                                    <ActiveIfP>- Cross browser compatiable wrapper called <a href="https://reactjs.org/docs/events.html">Synthetic Events</a> to the native browser event object.</ActiveIfP>
                                    <ActiveIfP>
                                        - If needed, Components proivde easy hooks to bind & unbind non element events.
                                        <CodePre>
                                            {
`class Foo extends React.Component {
    someEventHandler(e) {
        // do something with the event.
    }
    componentWillMount() {
        someEl.addEventListener('eventName', this.someEventHandler);
    }
    componentWillUnmount() {
        someEl.removeEventListener('eventName', this.someEventHandler);
    }
}
`
                                            }
                                        </CodePre>
                                    </ActiveIfP>
                                </ActiveIfContent>
                            </LiActivateIf>
                        </ul>
                    </li>
                    <li className={this.state.subPointer >= 2 ? `activated`: ``}>
                        <b>Communicating using this syntax</b>
                        <ul>
                            <LiActivateIf expectedPage={2} page={this.state.subPointer}>
                                <ActiveIfHeader>Now that we are familier with this syntax, what are the other uses?</ActiveIfHeader>
                                <ActiveIfContent>
                                    <ActiveIfP>- React strategy: everything is a react element.</ActiveIfP>
                                    <ActiveIfP>- Element Creation: React.createElement creates every element.</ActiveIfP>
                                    <ActiveIfP>- Element Creation: Props are supplied to all element creation expressions.</ActiveIfP>
                                    <ActiveIfP>- Custom Elements: Should obey the same rule</ActiveIfP>
                                </ActiveIfContent>
                            </LiActivateIf>

                            <LiActivateIf expectedPage={3} page={this.state.subPointer}>
                                <ActiveIfHeader>Defining event handlers on Custom Components</ActiveIfHeader>
                                <ActiveIfContent>
                                    <ActiveIfP>
                                        Since the syntax for handling the events has to obey the same rule, we need to add this support to our component.
                                        This is where the Props come in handy again.
                                    </ActiveIfP>
                                    <ActiveIfP>
                                        Props can accept callback functions, which can be called by the custom component to respond to some event.
                                    </ActiveIfP>
                                    <ActiveIfP>
                                        Callback can be as simple as deligating an internal event to a wrapper component.
                                        <CodePre>
{
`function CustomButton(props) {
    return <button onClick={props.onCustomClick}>My Custom Button</button>
}
`
}
                                        </CodePre>
                                    </ActiveIfP>
                                    <ActiveIfP>
                                        - OR -
                                    </ActiveIfP>
                                    <ActiveIfP>
                                        As custom as sending out events ( or callbacks ) on a custom state update.
                                        <CodePre>
{
`function CustomButton(props) {
    let i = 0;

    function sendClickOnEveryThreeClicks(e) {
        // push events only on every 3rd click
        i++;
        if ( i == 3 ) {
            i = 0;
            props.onCustomClick();
        }
    }

    return <button onClick={sendClickOnEveryThreeClicks}>My Custom Button</button>
}
`
}
                                        </CodePre>
                                    </ActiveIfP>

                                </ActiveIfContent>
                            </LiActivateIf>
                        </ul>
                    </li>
                </ol>
            </SlideContent>
        </Slide>
    }
}