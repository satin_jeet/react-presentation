import React from "react";
import { CodePre } from "../common/codepre";
import { LiActivateIf } from "./jsxslide";
import { ActiveIfHeader, ActiveIfContent, ActiveIfP } from "../common/activeliheader";

export function ComponentSlide(props: { activate: boolean, page: number }) {
    if (!props.activate) {
        return <li>Components</li>
    }

    return <li className="activated">
        <b>Components - <a href="https://reactjs.org/docs/components-and-props.html">https://reactjs.org/docs/components-and-props.html</a></b>
        <ul className={ props.page == undefined ? 'main-list': ''}>
            <LiActivateIf page={props.page} expectedPage={1}>
                <ActiveIfHeader>Components let you split the UI into independent, reusable pieces, and think about each piece in isolation.</ActiveIfHeader>
                <ActiveIfContent>
                    <ActiveIfP>What does it mean:</ActiveIfP>
                    <ActiveIfP>
                        Reusable piece: Component generally abstract a specific functionality specific to the UI inside it so it can be embedded & used in multiple places.
                    </ActiveIfP>
                    <ActiveIfP>
                        Independent: Component define their own interface with what they expect as input & what they render ( or return for rendering ), becoming independent 
                        in term of taking control of it's view piece. Parent components can only use the component in the ways allowed by the it's api.
                    </ActiveIfP>
                    <ActiveIfP>
                        Isolation: Components are functions in javascript which take some input as props and return a React Element to be rendered in the DOM. When writing the component, points mentioned above must be kept in mind. Adding Independent & reusable aspect
                        to the component code makes sure that this component can perform it's expected task regardless of what scenario it is used under.
                    </ActiveIfP>
                </ActiveIfContent>
            </LiActivateIf>
            <LiActivateIf page={props.page} expectedPage={2}>
                {
                    2 == props.page ? 
                        <ActiveIfHeader>Writing a Component</ActiveIfHeader>:
                        <ActiveIfHeader>How to Write a Component</ActiveIfHeader>
                }
                <ActiveIfContent>
                    <ActiveIfP>As a Javascript function</ActiveIfP>
                    <CodePre>
                        { 
                            "function MyComponent(props) {\n" +
                            "   return <h1>Hello, {props.name}</h1>;\n" +
                            "}"
                        }
                    </CodePre>
                </ActiveIfContent>
                <ActiveIfContent>
                    <ActiveIfP>As an ES6 class</ActiveIfP>
                    <CodePre>
                        { 
                            "class MyComponent extends React.Component {\n" +
                            "   constructor(props) {\n" +
                            "       super(props);\n" +
                            "   }\n" +
                            "   render() {\n" +
                            "       return <h1>Hello, {this.props.name}</h1>;\n" +
                            "   }\n" +
                            "}"
                        }
                    </CodePre>
                </ActiveIfContent>
            </LiActivateIf>

            <LiActivateIf page={props.page} expectedPage={3}>
                {
                    3 == props.page ? 
                        <ActiveIfHeader>Using / Rendering a component</ActiveIfHeader>:
                        <ActiveIfHeader>How to Use / Render a component</ActiveIfHeader>
                }
                <ActiveIfContent>
                    <ActiveIfP>React differentiates between a custom and native element based on how it is named.</ActiveIfP>
                    <ActiveIfP>Using a lower case tag name in the rendering statement tells react to render a native html element.</ActiveIfP>
                    <ActiveIfP>Using a camel case tag name tells react to render a custom React component.</ActiveIfP>
                    <CodePre>
                        { "// Rendering a native html element \n<div/> \n\n" }
                        { "// Rendering a custom component \n<MyComponent /> \n" }
                        { "// Rendering a custom component with props \n<MyComponent prop1=\"foo\"/> \n" }
                        { "<MyComponent prop1=\"foo\" prop2=\"bar\" /> \n" }
                        { 
`
// Rendering a custom component with children components
<MyComponent>
    <p>child 1</p>
    <p>child 2</p>
    ...
</MyComponent>` }
                    </CodePre>
                </ActiveIfContent>
            </LiActivateIf>
            <LiActivateIf page={props.page} expectedPage={4}>
                {
                    4 == props.page ? 
                        <ActiveIfHeader>Using components together</ActiveIfHeader>:
                        <ActiveIfHeader>How to use components together</ActiveIfHeader>
                }
                <ActiveIfContent>
                    <ActiveIfP>
                        For using the components together, JSX provides a natural & familier syntax. Simple use the render syntax to add a child component
                        to the parent on, as is done while writing html child elements under a parent element.
                    </ActiveIfP>
                    <ActiveIfP>
                        Both Native HTML element & React Component can act as parent and child element in the JSX UI definition syntax. F.E
                    </ActiveIfP>
                    <CodePre>
{`
<MainElement>
    <SomeReactElement />
    <p>This is a paragraph</p>
</MainElement>

// Or 
<p>
    <CustomElement>Some text under custom Element</CustomElement>
    <i>foo</i>
</p>
`}
                    </CodePre>
                </ActiveIfContent>
            </LiActivateIf>
        </ul>
    </li>
}