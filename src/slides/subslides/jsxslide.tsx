import React from "react";
import { CodePre } from "../common/codepre";
import { ActiveIfHeader, ActiveIfContent, ActiveIfP } from "../common/activeliheader";

export function JSXSlide(props: { activate: boolean, page: number }) {
    console.log(props);
    if (!props.activate) {
        return <li>JSX</li>
    }

    return <li className="activated">
        <b>JSX</b>
        <ul className={ props.page == undefined ? 'main-list': ''}>
            <LiActivateIf page={props.page} expectedPage={1}>
                <ActiveIfHeader>JSX is a syntax extension to JavaScript.</ActiveIfHeader>
                <ActiveIfContent>
                    <CodePre>
                        {
`const element = (
    <div>
        <h1>Hello!</h1>
        <h2>Good to see you here.</h2>
    </div>
);`
                        }
                    </CodePre>
                </ActiveIfContent>
            </LiActivateIf>
            <LiActivateIf page={props.page} expectedPage={2}>
                <ActiveIfHeader>JSX code is a valid expression.</ActiveIfHeader>
                <ActiveIfContent>
                    <ActiveIfP>Each JSX "Tag" creates a React Element, which is the smallest building block in React.</ActiveIfP>
                    <ActiveIfP>Elements can be passed to other elements.</ActiveIfP>
                    <CodePre>
                        {
                            "const element1 = <p>Hello World</p>;\n" +
                            "const element2 = <code>var foo = \"Hello World\"</code>;\n"
                        }
                    </CodePre>
                </ActiveIfContent>
            </LiActivateIf>
            <LiActivateIf page={props.page} expectedPage={3}>
                <ActiveIfHeader>JSX code is part of the component logic.</ActiveIfHeader>
                <ActiveIfContent>
                    <CodePre>
                        {
`
// Component to render a custom header
function CustomHeader(props) {
    return <div className="some-header-class">
        { props. children }
    </div>                            
}`
                        }
                    </CodePre>
                </ActiveIfContent>
            </LiActivateIf>
            <LiActivateIf page={props.page} expectedPage={4}>
                <ActiveIfHeader>JSX code is not required, but more helpful visually.</ActiveIfHeader>
                <ActiveIfContent>
                    <CodePre>
                        {
`
const element = (
    <h1 className="greeting">
        Hello, world!
    </h1>
);

// OR

const element = React.createElement(
    'h1',
    {className: 'greeting'},
    'Hello, world!'
);
`
                        }
                    </CodePre>
                </ActiveIfContent>
            </LiActivateIf>
            <LiActivateIf page={props.page} expectedPage={5}>
                <ActiveIfHeader>So what does JSX do exactly?</ActiveIfHeader>
                <ActiveIfContent>
                    <ActiveIfP>
                        JSX helps in writing code where we as a developer only focus on creating the view that we need
                        and write the logic around the view, without worrying about the syntax `React.createElement` to create the view Elements.
                    </ActiveIfP>
                    <ActiveIfP>
                        Since JSX is used with Javascript directly, logic and JSX can be used together and take advantage of each other to define the UI layout.
                    </ActiveIfP>
                    <CodePre>
{
`
// a simple JSX as a component
function Foo() {
    return <div>I am Foo</div>;
}


// Using JSX with JS variable.
// props: { a: number, b: number }
function Sum(props) {
    const a = props.a || 0;
    const b = props.b || 0;

    return <div>The Sum of {a} & {b} is { a + b }</div>
}


// Using JSX to render on a condition
// props: { hour: number }
function ShowGreeting(props) {
    let icon = null;
    let greeting = "";
    const hour = props.hour || 0;

    if (hour >= 4 && hour < 12) {
        icon = <SomeIconAboutMorning />;
        greeting = "Morning";
    } else if (hour >= 12 && hour < 5) {
        icon = <SomeIconAboutNoon />;
        greeting = "Afternoon";
    } else if (hour >= 5 && hour < 8) {
        icon = <SomeIconAboutEvening />;
        greeting = "Evening";
    } else {
        icon = <SomeIconAboutNight />;
        greeting = "Night";
    }

    return <div>
        { icon }
        Good { greeting }
    </div>
}


// render a react element if...
const renderFoo = false;
<div>
    {
        renderFoo && <Foo />
    }
</div>


// Loops
<div>
    {
        [1,2,3,4].map(n => <span>{n}</span>); // Using Arrow functions.
        // OR
        [1,2,3,4].map(function(n) {
            return <span>{n}</span>
        })
    }
</div>

/*=
<div>
    <span>1</span>
    <span>2</span>
    <span>3</span>
    <span>4</span>
</div>
*/
`
}
                    </CodePre>
                </ActiveIfContent>
            </LiActivateIf>
        </ul>
    </li>
}

export function LiActivateIf(props: { page: number, expectedPage?: number | number[], children: any }) {
    if (Array.isArray(props.expectedPage)) {
        return <li className={props.expectedPage.findIndex(ep => ep == props.page) >= 0 ? 'activated activated-li z-depth-2 row': 'not-activated row'}>
            { props.children }
        </li>
    } else {
        return <li className={props.page == props.expectedPage ? 'activated activated-li z-depth-2 row': 'not-activated row'}>
            { props.children }
        </li>
    }
}