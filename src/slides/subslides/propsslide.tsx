import React from "react";
import { CodePre } from "../common/codepre";
import { LiActivateIf } from "./jsxslide";
import { ActiveIfHeader, ActiveIfContent, ActiveIfP } from "../common/activeliheader";

export function PropsSlide(props: { activate: boolean, page: number }) {
    if (!props.activate) {
        return <li>Component Props & Data Flow</li>
    }

    return <li className="activated">
        <b>
            Component Props & Data Flow
        </b>
        <ul className={ props.page == undefined ? 'main-list': ''}>
            <LiActivateIf page={props.page} expectedPage={1}>
                <ActiveIfHeader>Component props define the signature of the component in terms of how and what data goes in and what and how data goes out.</ActiveIfHeader>
            </LiActivateIf>
            <LiActivateIf page={props.page} expectedPage={2}>
                <ActiveIfHeader>"Props" is the read only object passed to the component while it's creation.</ActiveIfHeader>
                <ActiveIfContent>
                    <ActiveIfP>
                        Components that recieve props are expected to behave as pure functions. If props need to be updated,
                        Copy the props to an internal variable and use that variable to track changes.
                    </ActiveIfP>
                    <CodePre className="col s6">
                        {
`// DON'T
function MyComponent(props) {
    props.name = "Hello " + props.name;
    return <b>{props.name}</b>
}

// DO
function MyComponent(props) {
    const str = "Hello " + props.name
    return <b>{ str }</b>
}\n\n\n\n\n`
                        }
                    </CodePre>
                    <CodePre className="col s6">
                        {
`// DON'T
class MyComponent extends React.Component{
    render() {
        this.props.name = "Hello " + this.props.name;
        return <b>{this.props.name}</b>
    }
}

// DO
class MyComponent extends React.Component{
    render() {
        const name = "Hello " + this.props.name
        return <b>Hello {name}</b>
    }
}`
                        }
                    </CodePre>
                </ActiveIfContent>
            </LiActivateIf>
            <LiActivateIf page={props.page} expectedPage={3}>
                <ActiveIfHeader>Anything that the component needs can be passed in the Props.</ActiveIfHeader>
                <ActiveIfContent>
                    <ActiveIfP>
                        Props can take in data in two ways:-
                    </ActiveIfP>
                    <ol>
                        <li>Pass data in as a <code>[key]: value</code> Pair on the component usage.</li>
                        <li>Pass data as a child to the component.</li>
                    </ol>
                    <CodePre className="col s6">
                        {
`// Define what component needs
// Pass name as props[name] key
function RenderNameInBold(props) {
    // needs a name in props
    return <b>{props.name}</b>
}

// use
<h1>
    <RenderNameInBold name="Satin"/>
</h1>\n\n
`
                        }
                    </CodePre>
                    <CodePre className="col s6">
                        {
`// Define what component needs
// Pass name as child component
function RenderNameInBold(props) {
    // needs a name in props
    return <b>{props.children}</b>
}

// use
<h1>
    <RenderNameInBold>
        Satin
    </RenderNameInBold>
</h1>
`
                        }
                    </CodePre>
                </ActiveIfContent>
            </LiActivateIf>
            <LiActivateIf page={props.page} expectedPage={4}>
                <ActiveIfHeader>Anything that needs to be emitted by the component can be requested via passing callback function in the props.</ActiveIfHeader>
                <ActiveIfContent>
                    <ActiveIfP>
                        Javascript passes around objects as references, but since react relies on object instances to check the changes happening in the Virtual DOM, it is highly advised against.
                        Instead we use callbacks to the child component, so that parent component can be shared data when needed via the callback.
                    </ActiveIfP>
                    <CodePre className="col s6">
                        {
`/**
 * Listen to a click event emitted by child component.
 *
 * props: {
 *     onBtnClicked: () => void;
 * }
 */
function SomeButtonComponent(props) {
    // Some fancy rendering on top of the button
    <button onClick={props.onBtnClicked}>Some Button</button>
}

// use
<SomeParent>
    <SomeButtonComponent onBtnClicked={functionToCallForTheClickEvent} />
</SomeParent>\n\n\n
`
                        }
                    </CodePre>
                    <CodePre className="col s6">
                        {
`/**
 * Listen to a value change event emitted by child component.
 *
 * props: {
 *     onFooChanged: () => void;
 * }
 */
class SomeComponent extends React.component{
    
    componentDidUpdate() {
        // Let the parent component know that foo has changed.
        this.props.onFooChanged(bar)
    }
}

// use
<SomeParent>
    <SomeComponent onFooChanged={functionToCallForTheChange} />
</SomeParent>
`
                        }
                    </CodePre>
                </ActiveIfContent>
            </LiActivateIf>
            <LiActivateIf page={props.page} expectedPage={5}>
                <ActiveIfHeader>Bonus: Do no always rely on the component user to provide the correct value.</ActiveIfHeader>
                <ActiveIfContent>
                    <ActiveIfP>
                        It is a good idea to Performing a basic check on the prop value or assign a default value to the missing props. It helps in reducing a lot of errors.
                        If possible, always use typechecks or prop types to ensure that the component is used in an expected manner.
                        React Provides PropTypes, Flow to enforce the type checks <a href="https://reactjs.org/docs/typechecking-with-proptypes.html">https://reactjs.org/docs/typechecking-with-proptypes.html</a>.
                        Typescript is also a very good tool to enforce compile type checks.
                    </ActiveIfP>
                    <CodePre className="col s12">
                        {
`// Missing prop check and value assignment
class SomeComponent extends React.component {
    
    render() {
        // if callBackParam does not exist, assign an empty function to avoid failure.
        const someCallBack = this.props.callBackParam || function() {};
        return <button onClick={someCallBack}>
            Button
        </button>
    }
}

// PropTypes check
import PropTypes from 'prop-types';

class SomeComponent extends React.component {
    
    render() {
        return <button onClick={this.props.callBackParam}>
            Button
        </button>
    }
}

SomeComponent.propTypes = {
    callBackParam: PropTypes.func
};
`
                        }
                    </CodePre>
                    <CodePre className="col s12" lang="tsx">
                        {
`// Using Typescript
interface ISomeComponentProps {
    callBackParam: () => {}
}

class SomeComponent extends React.component<ISomeComponentProps> {

    constructor(props: ISomeComponentProps) {
        super(props);
    }
    
    render() {
        return <button onClick={this.props.callBackParam}>
            Button
        </button>
    }
}

// <SomeComponent /> <-- Will throw compilation error as "callBackParam" is not supplied in the usage.
// <SomeComponent callBackParam={1}/> <-- Will throw compilation error as "callBackParam" is not a function.
<SomeComponent callBackParam={function() {}} /> // <-- Success.
`
                        }
                    </CodePre>
                </ActiveIfContent>
            </LiActivateIf>
        </ul>
    </li>
}