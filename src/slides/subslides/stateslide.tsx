import React from "react";
import { CodePre } from "../common/codepre";
import { LiActivateIf } from "./jsxslide";
import { ActiveIfHeader, ActiveIfContent, ActiveIfP } from "../common/activeliheader";

export function StateSlide(props: { activate: boolean, page: number }) {
    if (!props.activate) {
        return <li>Component State & Lifecycle Management</li>
    }

    return <li className="activated">
        <b>Component State & Lifecycle Management</b>
        <ul className={ props.page == undefined ? 'main-list': ''}>
            <LiActivateIf page={props.page} expectedPage={undefined}>
                <ActiveIfHeader>Lets take this example to learn state management & lifecycle of a react component</ActiveIfHeader>
                <ActiveIfContent>
                    <ActiveIfP>
                        Objective: Writing react code to display time i.e a simple Clock.
                    </ActiveIfP>
                    <CodePre className="col s12">
                        {
`
function tick() {
    const element = (
      <div>
        <h1>Hello, world!</h1>
        <h2>It is {new Date().toLocaleTimeString()}.</h2>
      </div>
    );
    ReactDOM.render(
      element,
      document.getElementById('root')
    );
  }
  
setInterval(tick, 1000);
`
                        }
                    </CodePre>
                    <ActiveIfP className="col s6">
                        Works just fine. But...
                    </ActiveIfP>
                </ActiveIfContent>
            </LiActivateIf>
            <LiActivateIf page={props.page} expectedPage={[1,2,3]}>
                <ActiveIfHeader>As a component.</ActiveIfHeader>
                {
                    props.page == 1 && 
                    <ActiveIfContent>
                        <ActiveIfP>
                            Hmmm.... Not good enought, Convert the clock into a component to take advantage of Virtual DOM.
                        </ActiveIfP>
                        <CodePre className="col s12">
                            {
    `
function Clock() {
    return <div>
        <h1>Hello, world!</h1>
        <h2>It is {new Date().toLocaleTimeString()}.</h2>
    </div>
}
function tick() {
    ReactDOM.render( <Clock />, document.getElementById('root') );
}

setInterval(tick, 1000);
`
                            }
                        </CodePre>
                    </ActiveIfContent>
                }
                {
                    props.page == 2 && 
                    <ActiveIfContent>
                        <ActiveIfP>
                            Hmmm.... still not good enought, Maybe Pass date to component to help vdom in state comparison..
                        </ActiveIfP>
                        <CodePre className="col s12">
                            {
`
function Clock(props) {
    return <div>
        <h1>Hello, world!</h1>
        <h2>It is {props.date.toLocaleTimeString()}.</h2>
    </div>
}
function tick() {
    ReactDOM.render( <Clock date={new Date()} />, document.getElementById('root') );
}

setInterval(tick, 1000);
`
                            }
                        </CodePre>
                    </ActiveIfContent>
                }
                {
                    props.page == 3 && 
                    <ActiveIfContent>
                        <ActiveIfP>
                            Nope, not any better. To use this clock as an independent clock, the parent component will always need to pass the date to the Clock component.
                        </ActiveIfP>
                        <CodePre>{`<p>\n    <Clock date={new Date()} />\n</p>`}</CodePre>
                        <ActiveIfP>
                            Not a very "Isolated" appraoch, is it. Time to use something that can be managed internally without too much handholding from the parent component. 
                        </ActiveIfP>
                    </ActiveIfContent>
                }
            </LiActivateIf>
            <LiActivateIf page={props.page} expectedPage={[4,5]}>
                <ActiveIfHeader>Stateful Component Cometh</ActiveIfHeader>
                <ActiveIfContent>
                    <ActiveIfP>
                        Stateful component can be called a more intelligent & interactive form of a component. Stateful component provides the ability to -
                    </ActiveIfP>
                    <ol>
                        <li>Maintain an internal data state</li>
                        <li>Re-render itself on demand.</li>
                        <li>Hook into it's state at different points in it's lifecycle.</li>
                    </ol>
                    <ActiveIfP>
                        So, lets convert the Clock to a stateful component.
                    </ActiveIfP>
                    <CodePre className="col s12">
                        {
`
class Clock extends React.Component {

    constrcutor(props) {
        super(props);
        // Start by defining the initial state
        this.state = {
            date: new Date
        }
`
+ 
    (props.page == 5 ?
`
        // Maintain an internal timer id
        this._timer = -1;
        this.updateState = this.updateState.bind(this);
    `: '')
    +

`
    }
`
    + 
    (props.page == 5 ? `
    componentDidMount() {
        this._timer = setTinterval(this.updateDate, 1000);
    }

    componentDidUnmount() {
        clearInterval(this._timer);
    }

    updateDate() {
        this.setState({ date: new Date() });
    }
    `: '')
    +
`
    render() {
        return <div>
            <div>Hello World</div>
            <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
        </div>
    }
}
`
                        }
                    </CodePre>
                </ActiveIfContent>
            </LiActivateIf>
            <LiActivateIf page={props.page} expectedPage={6}>
                <ActiveIfHeader>Managing component throughout it's lifecycle.</ActiveIfHeader>
                <ActiveIfContent>
                    <ActiveIfP>
                        React provided the following lifecycle methods to manage the internal state & do other things on top.
                    </ActiveIfP>
                    <ol>
                        <li><b>componentWillMount</b> - Acts as a suitable constructor </li>
                        <li><b>componentDidMount</b> - DOM is ready & mounted in the main document.</li>
                        <li><b>componentWillReceiveProps</b> - Parent component passed new Props to the component</li>
                        <li><b>shouldComponentUpdate</b> - Decide whether to update the component or not.</li>
                        <li><b>componentWillUpdate</b> - Execute If the update to the dom will happen</li>
                        <li><b>componentDidUpdate</b> - Execute after the update to the dom happens</li>
                        <li><b>componentWillUnmount</b> - Component will be removed from the main DOM & deleted.</li>
                    </ol>
                </ActiveIfContent>
            </LiActivateIf>
        </ul>
    </li>
}