import React from "react";
import { SlideHeader } from "./common/slide-header";
import { SlideContent } from "./common/slide-content";
import { Slide } from "./common/slide";
import { JSXSlide } from "./subslides/jsxslide";
import { ComponentSlide } from "./subslides/componentsslide";
import { PropsSlide } from "./subslides/propsslide";
import { StateSlide } from "./subslides/stateslide";

export function WhatReactOffers_JSX(props: any) {
    const {topic, subtopic} = props.match.params;

    let mainList = false;
    if (topic == undefined && subtopic === undefined) {
        mainList = true;
    }

    return <Slide>
        <SlideHeader>What react offers?</SlideHeader>
        <SlideContent>
            <ol className={'top-ol' + (mainList ? 'main-list': '')}>
                <JSXSlide activate={topic === 'jsx'} page={subtopic} />
                <ComponentSlide activate={topic === 'component'} page={subtopic} />
                <PropsSlide activate={topic==='props'} page={subtopic} />
                <StateSlide activate={topic==='state'} page={subtopic} />
                {/* <li>Define the View Layout, Interactions & Events Easily.</li>
                <li>Component Composition, Inheritance</li> */}
            </ol>
        </SlideContent>
    </Slide>
}