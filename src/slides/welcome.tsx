import React from "react";
import { SlideHeader } from "./common/slide-header";
import { SlideContent } from "./common/slide-content";
import { Slide } from "./common/slide";

export function Welcome() {
    return <Slide>
        <SlideHeader noIcon={true} className="big-header">
            React
            <small>by Facebook</small>
        </SlideHeader>
        <SlideContent>
            <span className="center-align">
                <p>This Demo is brought to you by :</p>
                <b>
                    <p>REACT: 16.8.6</p>
                    <p>REACT-DOM: 16.8.6</p>
                    <p>REACT ROUTER DOM: 5.0.0</p>
                    <p>TYPESCRIPT: 3.5.1</p>
                    
                    <p>SASS: 1.20.1</p>
                    <p>MATERIALIZE-CSS: 1.0.0-rc.2</p>
                    <p>PARCEL-BUNDLER: 1.12.3</p>
                </b>
            </span>
        </SlideContent>
    </Slide>
}