import React from "react";
import { SlideHeader } from "./common/slide-header";
import { SlideContent } from "./common/slide-content";
import { Slide } from "./common/slide";
import { ClockTower } from "./example/clocktower";
import { ActiveIfP, ActiveIfHeader, ActiveIfContent } from "./common/activeliheader";
import { LiActivateIf } from "./subslides/jsxslide";
import { CodePre } from "./common/codepre";

export class BasicExample extends React.Component {
    render() {
        return <Slide>
            <SlideHeader>Clock Tower</SlideHeader>
            <SlideContent>
                <ol className={'top-ol no-number'}>
                    <li className="activated">
                        <b></b>
                        <ul>
                            <LiActivateIf expectedPage={0} page={0}>
                                <ActiveIfHeader>Clock</ActiveIfHeader>
                                <ActiveIfContent>
                                    <ClockTower />
                                </ActiveIfContent>
                            </LiActivateIf>
                        </ul>
                    </li>
                </ol>
                <div className="row">
                    <div className="col s12">
                    <ul className="tabs" ref={(tabs) => {
                        tabs && M.Tabs.init(tabs);
                    }}>
                        <li className="tab col s3"><a className="active" href="#test1">Clock</a></li>
                        <li className="tab col s3"><a href="#test2">Clock Tower</a></li>
                    </ul>
                    </div>
                    <div id="test1" className="col s12">
                        <CodePre lang="tsx">
    {`
    import React, { Component } from "react";

    interface IClockProps {
        alarmHour: number,
        alarmMinute: number,
        onAlarm: (disableAlarm: () => void) => void;
    }

    export class Clock extends Component<IClockProps> {
        private _hour: number = 0;
        private _minute: number = 0;
        private timer = -1;

        state = {
            time: new Date(),
            alarmActive: false
        }

        constructor(props: IClockProps) {
            super(props);
            this._hour = props.alarmHour;
            this._minute = props.alarmMinute;
            console.log(\`Alarm requested at: \${this._hour}:\${this._minute}\`);
        }

        componentDidMount() {
            clearInterval(this.timer);
            this.setTimer();
        }

        componentWillUnmount() {
            clearInterval(this.timer);
        }

        componentWillUpdate() {
            const m = this.state.time.getMinutes();
            const h = this.state.time.getHours();

            if (!this.state.alarmActive) {
                if (m == this._minute && h == this._hour) {
                    console.log('Activating Alarm');
                    this.props.onAlarm(this.snooze);
                    this.setState({ alarmActive: true });
                }
            }
        }

        render() {
            return <div className="row">{
                    this.padLeft(this.state.time.getHours())
                }:{
                    this.padLeft(this.state.time.getMinutes())
                }:{
                    this.padLeft(this.state.time.getSeconds())
                } {this.state.alarmActive && <b>Alarm Active</b> }
            </div>
        }

        private snooze = () => {
            console.log('Snoozing Alarm');
            this._minute += 1;
            if (this._minute == 60) {
                this._hour++;
                this._minute++;

                if (this._minute == 24) {
                    this._minute = 0;
                }
            }
            this.setState({ alarmActive: false });
        }

        private setTimer = () => {
            this.timer = setInterval(() => {
                this.setState({ time: new Date() });
            }, 1000);
        }

        private padLeft(num: number): string {
            return \`0$\{num\}\`.slice(-2);
        }
    }
    `}
                        </CodePre>
                    </div>
                    <div id="test2" className="col s12">
                    <CodePre lang="tsx">
    {`
    import React from "react";
    import { Clock } from "./clock";

    export class ClockTower extends React.Component<any> {
        state: {
            alarmFired: boolean;
            disableFn: undefined | (() => void)
        } = {
            alarmFired: false,
            disableFn: undefined
        }

        private alarmDate = new Date(new Date().getTime() + 10000); 

        render() {

            return <div className="col s2">
                {/* Conditional Rendering */}
                {
                    this.state.alarmFired && <b className="red">Alarm !!</b>
                }
                
                <Clock alarmHour={this.alarmDate.getHours()} alarmMinute={this.alarmDate.getMinutes()} onAlarm={this.alarmHandler} />

                {
                    this.state.disableFn && <button onClick={() => {
                        this.state.disableFn && this.state.disableFn();
                        this.setState({ disableFn: undefined, alarmFired: false });
                    }}>Clear</button>
                }
            </div>
        }

        private alarmHandler = (disableFn: () => void) => {
            this.setState({ disableFn, alarmFired: true });
        }
    }
    `}
                    </CodePre>
                    </div>
                </div>
            </SlideContent>
        </Slide>
    }
}