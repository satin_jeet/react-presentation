import React, { useState } from "react";
import { render } from "react-dom";
import { Router, Route } from "react-router";
import { createBrowserHistory } from 'history'
import { Welcome } from "./slides/welcome";
import { WhatIsReact } from "./slides/whatisreact";
import { WhatReactIs } from "./slides/whatreactis";
import { WhatReactOffers_JSX } from "./slides/whatreactoffers";
import { AnimatedSwitch } from 'react-router-transition';
import { ComponentInteractions } from "./slides/interactions";
import { BasicExample } from "./slides/basicexample";
import "materialize-css/dist/js/materialize";

const history = createBrowserHistory();
let historyIndex = 0;

const pages: string[] = [
    "/",
    "/slide-2",
    "/slide-3",
    "/slide-4",
    "/slide-4/jsx",
    ...[1,2,3,4,5].map(n => "/slide-4/jsx/" + n),
    "/slide-4/component",
    ...[1,2,3,4].map(n => "/slide-4/component/" + n),
    "/slide-4/props",
    ...[1,2,3,4,5].map(n => "/slide-4/props/" + n),
    "/slide-4/state",
    ...[1,2,3,4,5,6].map(n => "/slide-4/state/" + n),
    "/slide-5",
    "/slide-example"
];
const backKeys = ["ArrowLeft", "Escape", "Backspace"];
const nextKeys = ["ArrowRight", "Space", " ", "Enter"];

function MainRouter() {
    (() => {
        console.log('Binding Event');
        historyIndex = pages.findIndex(p => p === history.location.pathname);
        document.addEventListener('keydown', (e: KeyboardEvent) => {
            if (backKeys.indexOf(e.key) >= 0) {
                historyIndex = historyIndex <= 0 ? 0 : historyIndex - 1;
            } else if (nextKeys.indexOf(e.key) >= 0) {
                historyIndex = historyIndex >= pages.length - 1 ? pages.length - 1: historyIndex + 1;
            }
            history.push(pages[historyIndex]);
        });
    })();

    return <Router history={history}>
        <AnimatedSwitch
            atEnter={{ opacity: 0 }}
            atLeave={{ opacity: 0 }}
            atActive={{ opacity: 1 }}
            className="switch-wrapper"
        >
            <Route exact={true} path="/" component={Welcome}></Route> 
            <Route exact={true} path="/slide-1" component={Welcome}></Route>   
            <Route exact={true} path="/slide-2" component={WhatIsReact}></Route>   
            <Route exact={true} path="/slide-3" component={WhatReactIs}></Route>
            <Route path="/slide-4/:topic?/:subtopic?" component={WhatReactOffers_JSX} />
            <Route exact={true} path="/slide-5" component={ComponentInteractions}></Route>
            <Route exact={true} path="/slide-example" component={BasicExample}></Route>
        </AnimatedSwitch>
    </Router>
}

// const MR = withRouter(MainRouter);

render(<MainRouter />, document.body.querySelector("#root"));

// import "prismjs";
